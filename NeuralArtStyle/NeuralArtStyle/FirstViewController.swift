//
//  FirstViewController.swift
//  NeuralArtStyle
//
//  Created by Valeria Bubnova on 26/05/16.
//  Copyright © 2016 Valeria Bubnova. All rights reserved.
//

import UIKit

extension UIImage {
    func resize(size: Float)-> UIImage {
        if size >= Float(self.size.width) {
            return self
        } else {
        let scale: Float = size/Float(self.size.width)
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: CGFloat(Float(self.size.width) * scale), height: CGFloat(Float(self.size.height)*scale))))
        imageView.contentMode = UIViewContentMode.ScaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, CGFloat(scale))
        imageView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
        }
    }
}

let max_size: Float = 100

class FirstViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet weak var imagePic: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: Upload image
    @IBAction func UploadStyle(sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.sourceType = .PhotoLibrary
        
        imagePickerController.delegate = self
        
        presentViewController(imagePickerController, animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        imagePic.image = selectedImage
        let resized_image = imagePic.image?.resize(max_size)
        if let data = UIImageJPEGRepresentation(resized_image!, 1) {
            let filename = getDocumentsDirectory().stringByAppendingPathComponent("image.jpg")
            data.writeToFile(filename, atomically: true)
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}

