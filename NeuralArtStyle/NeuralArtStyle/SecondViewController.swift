//
//  SecondViewController.swift
//  NeuralArtStyle
//
//  Created by Valeria Bubnova on 26/05/16.
//  Copyright © 2016 Valeria Bubnova. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet weak var stylePic: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: Upload image
    
    @IBAction func UploadStyle(sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.sourceType = .PhotoLibrary
        
        imagePickerController.delegate = self
        
        presentViewController(imagePickerController, animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        stylePic.image = selectedImage
        let resized_image = stylePic.image?.resize(max_size)
        if let data = UIImageJPEGRepresentation(resized_image!, 1) {
            let filename = getDocumentsDirectory().stringByAppendingPathComponent("style.jpg")
            data.writeToFile(filename, atomically: true)
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

}

