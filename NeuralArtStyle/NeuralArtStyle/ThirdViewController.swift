//
//  ThirdViewController.swift
//  NeuralArtStyle
//
//  Created by Valeria Bubnova on 26/05/16.
//  Copyright © 2016 Valeria Bubnova. All rights reserved.
//

import UIKit
import MobileCoreServices

func getDocumentsDirectory() -> NSString {
    let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
    let documentsDirectory = paths[0]
    return documentsDirectory
}

let filePath = getDocumentsDirectory() as String
let serverURL = "http://128.72.20.136:8000"
var task_name = ""

class ThirdViewController: UIViewController {
    @IBOutlet weak var iters: UILabel!
    @IBOutlet weak var resultPic: UIImageView!
    @IBOutlet weak var ButtonStop: UIButton!
    @IBOutlet weak var ButtonDraw: UIButton!
    @IBOutlet weak var ButtonCheck: UIButton!
    @IBOutlet weak var ButtonSave: UIButton!
    @IBOutlet weak var ProcessingIcon: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ProcessingIcon.hidden = true
    }
    
    // Saving to photo library
    
    @IBAction func SaveImage(sender: UIButton) {
        UIImageWriteToSavedPhotosAlbum(resultPic.image!, nil, nil, nil);
    }
    
    // Delete process
    func cancel() {
        do {
            try NSFileManager.defaultManager().removeItemAtPath(filePath + "/image.jpg")
        }
        catch let error as NSError {
            print("\(error)")
        }
        
        do {
            try NSFileManager.defaultManager().removeItemAtPath(filePath + "/style.jpg")
        }
        catch let error as NSError {
            print("\(error)")
        }
        resultPic.image = UIImage(named: "result")
        ButtonStop.hidden = true
        ButtonDraw.hidden = false
        ButtonCheck.hidden = true
        ButtonSave.hidden = false
        ProcessingIcon.hidden = true
        ProcessingIcon.stopAnimating()
    }
    
    @IBAction func Stop(sender: UIButton) {
        cancel()
        let url: NSURL = NSURL(string: serverURL + "/waiting/" + task_name)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "DELETE"
        
        let sesh = NSURLSession.sharedSession()
        let task: NSURLSessionDataTask = sesh.dataTaskWithRequest(request, completionHandler: {
            (data, response, error) in
            if response == nil {
                print("Timeout")
            }
            }
        )
        task.resume()
    }
    
    //Start drawing
    func mimeTypeForPath(path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream";
    }
    
    // POST request
    func startProcessing() {
        let url: NSURL = NSURL(string: serverURL + "/upload")!
        let request:NSMutableURLRequest = NSMutableURLRequest(URL:url)
        request.HTTPMethod = "POST"

        let random = Int(arc4random())
        task_name = String(random)
        let boundary = "Boundary-\(NSUUID().UUIDString)"
        let body = NSMutableData()
        
        let im_path = getDocumentsDirectory().stringByAppendingPathComponent("image.jpg")
        let im_url = NSURL(fileURLWithPath: im_path)
        let im_filename = im_url.lastPathComponent
        let im_data = NSData(contentsOfURL: im_url)!
        let im_mimetype = mimeTypeForPath(im_path)
        body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        body.appendData("Content-Disposition: form-data; name=\"file_im\"; filename=\"\(im_filename!)\"\r\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        body.appendData("Content-Type: \(im_mimetype)\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        body.appendData(im_data)
        body.appendData("\r\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        
        
        let st_path = getDocumentsDirectory().stringByAppendingPathComponent("style.jpg")
        let st_url = NSURL(fileURLWithPath: st_path)
        let st_filename = st_url.lastPathComponent
        let st_data = NSData(contentsOfURL: st_url)!
        let st_mimetype = mimeTypeForPath(st_path)
        body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        body.appendData("Content-Disposition: form-data; name=\"file_st\"; filename=\"\(st_filename!)\"\r\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        body.appendData("Content-Type: \(st_mimetype)\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        body.appendData(st_data)
        body.appendData("\r\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        
        
        body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        body.appendData("Content-Disposition: form-data; name=\"task_id\"\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        body.appendData("\(task_name)\r\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        body.appendData("--\(boundary)--\r\n".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        
        request.HTTPBody = body
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue("\(body.length)", forHTTPHeaderField: "Content-Length")
        print(request.debugDescription)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            if error != nil {
                print(error)
                return
            }
            do {
                if let responseDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                    print("success == \(responseDictionary)")
                }
            } catch {
                print(error)
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
            }
        }
        task.resume()
    }

    @IBAction func DrawPicture(sender: UIButton) {
        if NSFileManager.defaultManager().fileExistsAtPath(filePath + "/image.jpg") && NSFileManager.defaultManager().fileExistsAtPath(filePath + "/style.jpg") {
            ButtonStop.hidden = false
            ButtonDraw.hidden = true
            ButtonCheck.hidden = false
            ButtonSave.hidden = true
            ProcessingIcon.hidden = false
            ProcessingIcon.startAnimating()
            resultPic.image = UIImage(named: "wait")
            startProcessing()
            print(task_name)
        }
    }
    
    
    // Check if ready
    func get_result() {
        let url: NSURL = NSURL(string: serverURL + "/get_result/" + task_name)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        
        let sesh = NSURLSession.sharedSession()
        let task: NSURLSessionDataTask = sesh.dataTaskWithRequest(request, completionHandler: {
            (data, response, error) in
            if response == nil {
                print("Timeout")
            }
            }
        )
        task.resume()
        let pic_url = NSURL(string: serverURL + "/" + task_name)
        let imageData = NSData(contentsOfURL: pic_url!)
        resultPic.image = UIImage(data: imageData!)
    }
    
    
    @IBAction func check(sender: UIButton) {
        let url: NSURL = NSURL(string: serverURL + "/waiting/" + task_name)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        
        let sesh = NSURLSession.sharedSession()
        let task: NSURLSessionDataTask = sesh.dataTaskWithRequest(request, completionHandler: {
            (data, response, error) in
            if response == nil {
                print("Timeout")
            } else {
                if String(data: data!, encoding: NSUTF8StringEncoding) == "success" {
                    self.cancel()
                    self.get_result()
                    self.ButtonSave.hidden = false
                    self.iters.text = " "
                } else if String(data: data!, encoding: NSUTF8StringEncoding) == "error" {
                    self.cancel()
                    self.iters.text = " "
                } else {
                    self.iters.text = "Done: \(String(data: data!, encoding: NSUTF8StringEncoding))/500"
                }
                print(String(data: data!, encoding: NSUTF8StringEncoding))
            }
            }
        )
        task.resume()
    }
}
