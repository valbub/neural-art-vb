import os
from celery import Celery
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from werkzeug.utils import secure_filename
from time import sleep, time
from celery.exceptions import Terminated
from subprocess import Popen, PIPE

UPLOAD_FOLDER = 'images'
OUTPUT_FOLDER = 'animation'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
ITER = '500'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['OUTPUT_FOLDER'] = OUTPUT_FOLDER
app.config['ALLOWED_EXTENSIONS'] = ALLOWED_EXTENSIONS
app.config['BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'
app.config['BROKER_TRANSPORT'] = 'redis'

celery = Celery('routes', broker=app.config['BROKER_URL'])
celery.conf.update(app.config)


def allowed_file(filename):
    return '.' in filename and filename.rpartition('.')[2] in app.config['ALLOWED_EXTENSIONS']


def remove_files(task_id):
    if os.path.exists(os.path.join(OUTPUT_FOLDER, task_id)):
        os.rmdir(os.path.join(OUTPUT_FOLDER, task_id))
    if os.path.isfile(os.path.join(UPLOAD_FOLDER, task_id + "_im.jpg")):
        os.remove(os.path.join(UPLOAD_FOLDER, task_id + "_im.jpg"))
    if os.path.isfile(os.path.join(UPLOAD_FOLDER, task_id + "_st.jpg")):
        os.remove(os.path.join(UPLOAD_FOLDER, task_id + "_st.jpg"))


@celery.task(bind=True, throws=Terminated)
def repaint(self, subject, style, output, task_name):
    try:
        print ("python neural_artistic_style.py --subject images/%s --style images/%s --output %s "
         "--iterations %s --animation %s" % (
             subject, style, output, str(int(ITER) + 1), os.path.join(OUTPUT_FOLDER, output)))
        neural_as = Popen(
        ["python neural_artistic_style.py --subject images/%s --style images/%s --output %s "
         "--iterations %s --animation %s" % (
             subject, style, output, str(int(ITER) + 1), os.path.join(OUTPUT_FOLDER, output))], shell=True,
        stdout=PIPE)
    except Exception as e:
        self.update_state(task_id=task_name, state='FAILED', meta={'iteration': "0"})
    for output in neural_as.stdout:
        iter = output.decode().strip()
        self.update_state(task_id=task_name, state='PROGRESS', meta={'iteration': iter})
        if iter == ITER:
            break
    self.update_state(task_id=task_name, state='SUCCESS', meta={'iteration': ITER})


@app.route('/waiting/<task_id>', methods=['GET'])
def get_status(task_id):
    if celery.AsyncResult(task_id).state == "SUCCESS":
        print("started")
        send_from_directory(app.config['OUTPUT_FOLDER'], task_id + "-" + ITER + ".jpg")
        return "success"
    elif celery.AsyncResult(task_id).state == "ERROR":
        return "error"
    else:
        print("started")
        if celery.AsyncResult(task_id).state != "PENDING":
            return celery.AsyncResult(task_id).result.info['iteration']
        return "error"


@app.route('/get_result/<task_id>', methods=['GET'])
def get_result(task_id):
    print("accept")
    return send_from_directory(app.config(os.path.join('OUTPUT_FOLDER', task_id), task_id + "-" + ITER + ".jpg"))


@app.route('/upload', methods=['POST'])
def upload():
    print("accept")
    image = request.files['file_im']
    print("accept")
    style = request.files['file_st']
    print("accept")
    task_name = request.form['task_id']
    print("accept")
    if image and allowed_file(image.filename) and style and allowed_file(style.filename):
        print("accept")
        imagename = task_name + "_im.jpg"
        print(imagename)
        stylename = task_name + "_st.jpg"
        print(stylename)
        image.save(os.path.join(UPLOAD_FOLDER, imagename))
        print("save")
        style.save(os.path.join(UPLOAD_FOLDER, stylename))
        print("save")
        new_folder = os.path.join(OUTPUT_FOLDER, task_name)
        print(new_folder)
        if not os.path.exists(new_folder):
            os.mkdir(new_folder)
            print("mkdir")
        outputname = task_name + ".jpg"
        print (outputname)
        repaint.apply_async([imagename, stylename, outputname, task_name], task_id=task_name)
        print (outputname)
        return "200"


@app.route('/waiting/<task_id>', methods=['DELETE'])
def stop_processing(task_id):
    print(task_id)
    print(celery.AsyncResult(task_id).state)
    if celery.AsyncResult(task_id).state != "SUCCESS":
        celery.control.revoke(task_id, terminate=True)
        remove_files(task_id=task_id)
    return "200"


@app.route('/')
def home():
    return render_template('home.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000)
    debug = True
